<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('firstname', TextType::class, [
                'label'=>'First Name'
            ])
            ->add('lastname', TextType::class, [
                'label'=>'Last Name'
            ])
            ->add('email', TextType::class, [
                'label'=>'Email'
            ])
            ->add('phone', TextType::class, [
                'label'=>'Phone'
            ])
            ->add('message', TextType::class, [
                'label'=>'Message'
            ])


        ;
    }
    public function getBlockPrefix()
    {
        return '';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'method'=>'get',
            'csrf_protection'=>false
        ]);
    }


}
