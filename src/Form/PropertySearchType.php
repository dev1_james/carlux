<?php

namespace App\Form;

use App\Entity\Option;
use App\Entity\Property;
use App\Entity\PropertySearch;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertySearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('model', TextType::class, [
                'required' => false,
                'label'=> 'Model',
                'attr'=>[
                    'placeholder'=>'Model'
                ],

            ])
            ->add('carCondition',ChoiceType::class, [
                'choices'=> $this->getChoices(),
                'label'=> 'Condition',
                'required'=>false
            ])
            ->add('minKilometer', IntegerType::class, [
                'required' => false,
                'label'=> 'Min Kilometer',
                'attr'=>[
                    'placeholder'=>'min Kilometer'
                ]
            ])
            ->add('maxKilometer', IntegerType::class, [
                'required' => false,
                'label'=> 'Max Kilometer',
                'attr'=>[
                    'placeholder'=>'min Kilometer'
                ]
            ])
            ->add('maxPrice', IntegerType::class, [
                'required' => false,
                'label'=> 'Max Price',
                'attr'=>[
                    'placeholder'=>'max Price'
                ]
            ])
            ->add('minPrice', IntegerType::class, [
                'required' => false,
                'label'=> 'Min Price',
                'attr'=>[
                    'placeholder'=>'min Kilometer'
                ]
            ])
            ->add('options', EntityType::class, [
                'required' => false,
                'label'=> 'Options',
                'class'=> Option::class,
                'choice_label'=>'name',
                'multiple'=>true
            ])
            /*->add('fabricant', EntityType::class, [
                'required' => false,
                'label'=> 'Manufacturer',
                'class'=> Property::class,
                'choice_label'=> 'fabricant',
                'multiple'=>false
            ])*/

            ->add('fabricant', TextType::class, [
                'required' => false,
                'label'=> 'Manufacturer',
                'attr'=>[
                    'placeholder'=>'Manufacturer'
                ]
            ])
            ->add('sortby', ChoiceType::class, [
                'choices'=> $this->getSort(),
                'label'=> 'Sort by',
                'required'=>false
            ])

        ;
    }
    public function getBlockPrefix()
    {
        return '';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PropertySearch::class,
            'method'=>'get',
            'csrf_protection'=>false
        ]);
    }

    private function getChoices()
    {
        $choices = PropertySearch::CARCONDITION;
        $output = [];
        foreach ($choices as $k => $v){
            $output[$v] = $k;
        }
        return $output;
    }

    private function getSort()
    {
        $choices = PropertySearch::SORTBY;
        $output = [];
        foreach ($choices as $k => $v){
            $output[$v] = $k;
        }
        return $output;
    }
}
