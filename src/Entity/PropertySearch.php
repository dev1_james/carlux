<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

class PropertySearch
{

    const CARCONDITION=[
        1=>'Used',
        2=>'New'
    ];
    const SORTBY=[
        0=>'date',
        1=>'price',
        2=>'kilometre',
        3=>'year'
    ];

    /**
     * @var String | null
     */
    private $fabricant;

    private $model;

    /**
     * @var int| null
     */
    private  $maxPrice;

    /**
     * @var ArrayCollection
     */
    private $options;

    /**
     * @var int | null
     */
    private $minKilometer;

    /**
     * @var int | null
     */
    private $maxKilometer;

    /**
     * @var int | null
     */
    private $minPrice;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $carCondition;
    /**
     * @var string | null
     */
    private $sortby;

    /**
     * @return string|null
     */
    public function getSortby(): ?string
    {
        return $this->sortby;
    }

    /**
     * @param string|null $sortby
     * @return PropertySearch
     */
    public function setSortby(?string $sortby): PropertySearch
    {
        $this->sortby = $sortby;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getCarCondition()
    {
        return $this->carCondition;
    }

    /**
     * @param mixed $carCondition
     * @return PropertySearch
     */
    public function setCarCondition($carCondition)
    {
        $this->carCondition = $carCondition;
        return $this;
    }


    public function __construct()
    {
        $this->options = new ArrayCollection();
    }


    /**
     * @return int|null
     */
    public function getMaxPrice(): ?int
    {
        return $this->maxPrice;
    }

    /**
     * @param int|null $maxPrice
     */
    public function setMaxPrice(?int $maxPrice): void
    {
        $this->maxPrice = $maxPrice;
    }

    /**
     * @return ArrayCollection
     */
    public function getOptions(): ArrayCollection
    {
        return $this->options;
    }

    /**
     * @param ArrayCollection $options
     */
    public function setOptions(ArrayCollection $options): void
    {
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    public function getMinKilometer()
    {
        return $this->minKilometer;
    }

    /**
     * @param mixed $minKilometer
     */
    public function setMinKilometer($minKilometer): void
    {
        $this->minKilometer = $minKilometer;
    }

    /**
     * @return mixed
     */
    public function getMaxKilometer()
    {
        return $this->maxKilometer;
    }

    /**
     * @param mixed $maxKilometer
     * @return PropertySearch
     */
    public function setMaxKilometer($maxKilometer)
    {
        $this->maxKilometer = $maxKilometer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinPrice()
    {
        return $this->minPrice;
    }

    /**
     * @param mixed $minPrice
     * @return PropertySearch
     */
    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;
        return $this;
    }
    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     * @return PropertySearch
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return String|null
     */
    public function getFabricant(): ?String
    {
        return $this->fabricant;
    }

    /**
     * @param String|null $fabricant
     * @return PropertySearch
     */
    public function setFabricant(?String $fabricant): PropertySearch
    {
        $this->fabricant = $fabricant;
        return $this;
    }

}