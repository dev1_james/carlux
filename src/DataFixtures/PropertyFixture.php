<?php

namespace App\DataFixtures;

use App\Entity\Property;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class PropertyFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 50; $i++){
            $property = new Property();
            $property->setTitle('brand new car '.$i);;
    }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
