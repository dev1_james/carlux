<?php
namespace App\Controller\Client;
use App\Entity\Client;
use App\Entity\Property;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ClientProperty extends AbstractController {


    /**
     * @var ClientRepository
     */
    private ClientRepository $repository;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    public function __construct(ClientRepository $repository, EntityManagerInterface $em)
    {

        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/profile/{slug}-{id}", name="client.profile",  requirements={"slug": "[a-z0-9\-]*"})
     * @param Client $client
     * @param string $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public  function profile(Client $client, string $slug){
        if($client->getSlug() !== $slug){
            return $this->redirectToRoute('client.profile',[
                'id' => $client->getId(),
                'slug'=> $client->getSlug()
            ], 301);
        }

        return $this->render('client/profile.html.twig', [
            "client"=> $client,
            "current_menu" => "properties"
        ]);
    }

    /**
     * @Route("profile/favorite/{slug}-{id}", name="client.favorite", requirements={"slug": "[a-z0-9\-]*"})
     * @param Property $property
     * @param Request $request
     * @return Response
     */
    public function addFavorite(Property $property, Request $request) : Response
    {
        $client = $this->getUser();
        if($this->getUser()){
            $client->addClientProperty($property);
            $this->em->flush();
            $this->addFlash('success', 'The car is added to your favorites');
            return $this->redirectToRoute('property.show', [
                'id' => $client->getId(),
                'slug'=> $client->getSlug()
            ]);
        }
        else{
            $this->addFlash('success', 'This action require to be logged first');
            return $this->redirectToRoute('user.login');
        }


    }


    /**
     * @Route("profile/favorite/{id}", name="favorite.remove")
     * @param Property $property
     * @param Request $request
     * @return Response
     */
    public function removeFavorite(Property $property, Request $request) : Response
    {
        $client = $this->getUser();
        if($this->getUser()){
            $client->removeClientProperty($property);
            $this->em->flush();
            $this->addFlash('success', 'The item has been removed properly');
            return $this->redirectToRoute('client.profile', [
                'id' => $property->getId(),
                'slug'=> $property->getSlug()
            ]);
        }
        else{
            $this->addFlash('success', 'This action require to be logged first');
            return $this->redirectToRoute('user.login');
        }


    }


}