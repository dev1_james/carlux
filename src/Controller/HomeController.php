<?php
namespace App\Controller;
use App\Entity\Client;
use App\Repository\ClientRepository;
use App\Repository\PropertyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="home")
     * @param PropertyRepository $repository
     * @return Response
     */
    public function index(PropertyRepository $repository, EntityManagerInterface $em): Response
    {
        $properties = $repository->findLatest();
        $admin = $this->getUser();
        $client = $this->getUser();
        
        return $this->render('pages/Home.html.twig', [
            'properties' => $properties,
            'client'=> $client,
            'admin'=>$admin
            ]);
    }
}