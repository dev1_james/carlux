<?php


namespace App\Controller;


use App\Entity\Admin;
use App\Entity\Client;
use App\Form\AdminType;
use App\Form\ClientType;
use App\Repository\AdminRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 * @package App\Controller
 * @UniqueEntity(fields={"email"}, message="This email is already registred")
 */
class SecurityController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;


    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $encoder;
    /**
     * @var AdminRepository
     */
    private AdminRepository $repository;

    public function __construct(AdminRepository $repository, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder){
        $this->em = $em;
        $this->encoder  = $encoder;
        $this->repository = $repository;
    }
    /**
     * @Route("login", name="user.login")
     *
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        if ($this->isGranted('ROLE_USER') == false) {
            $error = $authenticationUtils->getLastAuthenticationError();
            $lastUsername = $authenticationUtils->getLastUsername();
            return $this->render('security/Userlogin.html.twig', [
                'last_username'=>$lastUsername,
                'error'=> $error
            ]);
        }
        return $this->redirectToRoute("home");


    }


    /**
     * @Route("admin/register", name="admin.register")
     * @param Request $request
     * @return Response
     */
    public function adminRegister(AdminRepository $repository,Request $request){
        if($this->isGranted('ROLE_ADMIN')== false)
        {
            $admin = new Admin();
            $form = $this->createForm(AdminType::class, $admin);
            $form->handleRequest($request);
            $errors = $form->getErrors();
            if($form->isSubmitted() && $form->isValid()){
                $admin->setPassword($this->encoder->encodePassword($admin,$admin->getPassword()));
                if($repository->countAdmin() === 0){
                    $admin->setType("2");
                    $admin->setStatus(1);
                }
                try {
                    $this->em->persist($admin);
                } catch (UniqueConstraintViolationException $e) {
                    $form->get('$admin->getEmail()')->addError(new FormError('This code is already taken'));
                }

                $this->em->flush();
                $this->addFlash('success', 'you are now able to login ');
                return $this->redirectToRoute('app_login');
            }

            return $this->render('security/register.html.twig', [
                'admin'=>$admin,
                'errors'=> $errors,
                'form'=>$form->createView()
            ]);
        }
        return $this->redirectToRoute("admin.property.index");

    }

    /**
     * @Route("client/register", name="client.register")
     * @param Request $request
     * @return Response
     */
    public function Clientregister(Request $request){
        $var = rand(1,5000);
        $client = new Client();
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()){
            $client->setPassword($this->encoder->encodePassword($client,$client->getPassword()));
            $client->setUsername($client->getName().$client->getSurname().($var));
            $client->setRoles($client->getRoles());
            $this->em->persist($client);
            $this->em->flush();
            $this->addFlash('success', 'you are now able to login with username : '.$client->getUsername());
            return $this->redirectToRoute('user.login');

        }
        return $this->render('security/clientRegister.html.twig', [
            'admin'=>$client,
            'form'=>$form->createView()
        ]);
    }

}