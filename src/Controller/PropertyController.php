<?php
namespace App\Controller;
use App\Entity\Contact;
use App\Entity\Property;
use App\Entity\PropertySearch;
use App\Form\ContactType;
use App\Form\PropertySearchType;
use App\Notification\ContactNotification;
use App\Repository\PropertyRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine \ ORM \ EntityManagerInterface;

class PropertyController extends AbstractController
{

    /**
     * @var PropertyRepository
     */
    private PropertyRepository $repository;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;


    public function __construct(PropertyRepository $repository, EntityManagerInterface $em)
    {

        $this->repository = $repository;

        $this->em = $em;
    }

    /**
     * @Route("/cars", name="property.index")
     * @return Response
     */
    public  function index(PaginatorInterface $paginator, Request $request): Response
    {

        $search = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);

        $properties = $paginator->paginate(
            $this->repository->findAllVisibleQuery($search),
            $request->query->getInt('page',1),
            6
        );

        $client = $this->getUser();

        return $this->render('property/index.html.twig', [
            'current_menu'=>'properties',
            'properites'=> $properties,
            'form'=> $form->createView(),
            "client"=> $client
        ]);
    }

    /**
     * @Route("/cars/{slug}-{id}", name="property.show", requirements={"slug": "[a-z0-9\-]*"})
     * @param Property $property
     * @return Response
     */

    public function show(Property $property, string $slug, Request $request, ContactNotification $notification) : Response
    {

        $client = $this->getUser();
        if($property->getSlug() !== $slug){
            return $this->redirectToRoute('property.show',[
                'id' => $property->getId(),
                'slug'=> $property->getSlug()
            ], 301);
        }
        $contact = new Contact();
        $form = $this->createForm(ContactType::class);
        $contact->setProperty($property);
        $form->handleRequest($request);

        if($form->isSubmitted() and $form->isValid()){
            $notification->notify($contact);
            $this->addFlash('success','Your Email has been successfully Send ');
            return $this->redirectToRoute(
                'property.show',[
                    'id' => $property->getId(),
                    'slug'=> $property->getSlug()
                ]);
        }
        return $this->render('property/show.html.twig', [
            "property"=> $property,
            "current_menu" => "properties",
            "client"=> $client,
            "form"=> $form->createView()
        ]);
    }


}