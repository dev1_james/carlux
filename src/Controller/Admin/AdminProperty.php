<?php
namespace App\Controller\Admin;

use App\Entity\Admin;
use App\Entity\Property;
use App\Form\PropertyType;
use App\Repository\PropertyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;


class AdminProperty extends AbstractController{

    /**
     * @var PropertyRepository
     */
    private PropertyRepository $repository;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;


    public function __construct(PropertyRepository $repository, EntityManagerInterface $em)
    {

        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin", name="admin.property.index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginatorInterface $paginator, Request $request){
        $admin = $this->getUser();

        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        // or add an optional message - seen by developers
        //$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        if($admin->getType()=="2" and $admin->getStatus()== true){
            $properties = $paginator->paginate(
                $this->repository->findAll(),
                $request->query->getInt('page',1),
                6);
            return $this->render('admin/property/index.html.twig', compact('properties','admin'));
        }
        elseif ($admin->getStatus()==false){
            return $this->render('admin/property/index2.html.twig', [
                "admin"=>$admin
            ]);
        }
        else{
            $properties = $paginator->paginate(
                $this->repository->findAdmin($admin->getId()),
                $request->query->getInt('page',1),
                6);
            return $this->render('admin/property/index.html.twig', compact('properties','admin'));
        }


    }

    /**
     * @Route("admin/car/create", name="admin.property.new")
     */
    public function new(Request $request)
    {
        $admin = $this->getUser();
        $property = new Property();
        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $admin->addProperty($property);
            $this->em->persist($property);
            $this->em->flush();
            $this->addFlash('success', 'Entries have been recorded successfuly!');
            return $this->redirectToRoute('admin.property.index');
        }
        return $this->render('admin/property/new.html.twig', [
            'property'=>$property,
            'form'=>$form->createView(),
            'admin'=>$admin
        ]);
    }

    /**
     * @Route("/admin/car/{id}", name="admin.property.edit", methods="GET|POST")
     * @param Property $property
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit (Property $property, Request $request)
    {
        $admin = $this->getUser();

        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->em->flush();
            $this->addFlash('success', 'well modify with succes');
            return $this->redirectToRoute('admin.property.index');

        }

        return $this->render('admin/property/edit.html.twig', [
            'property'=>$property,
            'form'=>$form->createView(),
            'admin'=>$admin
        ]);
    }

    /**
     * @Route("/admin/car/{id}", name="admin.property.delete", methods="DELETE")
     * @param Property $property
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function delete(Property $property, Request $request)
    {
        if ($this->isCsrfTokenValid('delete-item', $request->request->get('_token'))){
            $this->em->remove($property);
            $this->em->flush();
            $this->addFlash('success', 'Well Delete');
        }


        return $this->redirectToRoute('admin.property.index');

    }
}