<?php


namespace App\Controller\Admin;


use App\Entity\Admin;
use App\Form\AdminEditType;
use App\Form\AdminType;
use App\Repository\AdminRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SuperAdminProperty extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var AdminRepository
     */
    private AdminRepository $repository;

    public  function __construct(AdminRepository $repository, EntityManagerInterface $em)
    {

        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     * @Route("admin/managment", name="admin.management")
     *
     */
    public function index(){
        $admin = $this->getUser();
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $admins = $this->repository->findAdmins()->getResult();
        return $this->render('admin/managment/index.html.twig', compact('admins','admin'));

    }

    /**
     * @Route("admin/users/{id}", name="admin.managment.edit", methods="GET|POST")
     * @param Admin $admin
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
   public function edit(Admin $admin, Request $request){

       $form= $this->createForm(AdminEditType::class, $admin);
       $form->handleRequest($request);
       if($form->isSubmitted() && $form->isValid()){
           $this->em->flush();
           $this->addFlash('success', 'well modify with succes');
           return $this->redirectToRoute('admin.management');

       }
       return $this->render('admin/managment/edit.html.twig', [
           'admin'=> $admin,
           'form'=>$form->createView()
       ]);

   }

}