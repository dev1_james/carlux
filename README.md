# Symfony 4 par l'exemple 
Ceci est un repository qui illustre un exemple de l'utilisation de Symfony 4

Après avoir clôné ce repository, modifiez le fichier .env et remplacer la ligne ci-dessous par votre accès à la base de données

DATABASE_URL=mysql://root:@127.0.0.1:3306/caragence
Pour installer les différents packages nécessaire au fonctionnement de l'application

$ composer install
Lancez le serveur

$ php bin/console server:run
Générez les fixtures

$ php bin/console doctrine:fixtures:load --append
Pour accèder à l'administration, aller sur '/admin' puis rentrez les identifiants admin@mail.com:admin

# Enjoy!!!